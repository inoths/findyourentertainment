package org.webapps.FindYourEntertainment.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.xpath;

import java.util.Locale;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.webapps.FindYourEntertainment.model.Login;
import org.webapps.FindYourEntertainment.service.LoginService;
import org.webapps.FindYourEntertainment.service.exception.UserNotExistsException;

@RunWith(SpringRunner.class)
@WebMvcTest(LoginController.class) //indicates which controller will be tested
public class LoginControllerTest {

	private static final String PATH_LOGIN_FORM = "/login";
	private static final String PATH_CONFIRMATION_PAGE = "/thank-you";
	
	private static final String FORM_FIELD_USERNAME = "userName";
	private static final String FORM_FIELD_PASSWORD = "password";
	
	private static final String VIEW_LOGIN_FORM = "login";
	
	private static final String CONTENTTYPE_HTML_UTF8 = "text/html;charset=UTF-8";
	
	@Autowired
	private MockMvc mvc;
	
	@MockBean
	private LoginService loginService;
	
	private ResultActions result;
	
	private void given_theUserIsOnTheLoginPage() throws Exception {
		// @formatter:off
		 result = mvc.perform(
				 get(PATH_LOGIN_FORM)
				 .locale(Locale.ENGLISH))
				 
				 .andDo(print())	//display for debug
				 .andExpect(content().contentType(CONTENTTYPE_HTML_UTF8))
				 .andExpect(status().isOk())
				 .andExpect(view().name(VIEW_LOGIN_FORM));
		// @formatter:on
	}
	
	private void then_theFormContains(final Login user) throws Exception {
		// @formatter:off
		result.andExpect(xpath("//input[@name='" + FORM_FIELD_USERNAME + "']/@value").string(user.getUserName()));
		// @formatter:on
	}
	
	@Test
	public void testLoginFormIsEmptyWhenLoginFormIsOpened() throws Exception {
		given_theUserIsOnTheLoginPage();
		then_theFormContains(new Login("", ""));
		then_loginIsNotSentToTheService();
	}
	
	private void when_userSubmitsLoginFormContaining(final Login loginFormInput) throws Exception {
		// @formatter:off
		result = mvc.perform(
				post(PATH_LOGIN_FORM)
				.param(FORM_FIELD_USERNAME, loginFormInput.getUserName())
				.param(FORM_FIELD_PASSWORD, loginFormInput.getPassword())); 
		// @formatter:on

	}
	
	private void then_theUserIsRedirectedToTheConfirmationPage() throws Exception {
		result.andDo(print())
			  .andExpect(status().is3xxRedirection())
			  .andExpect(redirectedUrl(PATH_CONFIRMATION_PAGE));
	}
	
	@Test
	public void testLoginFormTakesCorrectlySubmittedData() throws Exception {
		given_theUserIsOnTheLoginPage();
		Login validLoginInput = new Login("Adam", "12345");
		when_userSubmitsLoginFormContaining(validLoginInput);
		then_loginIsSentToTheService(validLoginInput);
		then_theUserIsRedirectedToTheConfirmationPage();
	}
	
	private void then_theLoginIsRefusedDueToBadRequest() throws Exception {
		result.andDo(print()) 		//for debug
			  .andExpect(status().isBadRequest())
			  .andExpect(xpath("//div[@class='formErrorMessage']").exists());
	}
	
	private void then_fieldErrorDisplayedFor(String fieldName) throws Exception {
		result.andExpect(xpath("//div[@class='formErrorMessage']/following-sibling::*[position()=1][@name='" + fieldName + "']").exists());
	}
	
	private void then_fieldErrorNotDispalyedFor(String fieldName) throws Exception {
		result.andExpect(xpath("//div[@class='formErrorMessage']/following-sibling::*[position()=1][@name='" + fieldName + "']").doesNotExist());
	}
	
	@Test
	public void testLoginFormRefusesInputWithMissingUserName() throws Exception {
		given_theUserIsOnTheLoginPage();
		final Login loginWithMissingUserName = new Login("", "12345");
		when_userSubmitsLoginFormContaining(loginWithMissingUserName);
		then_theLoginIsRefusedDueToBadRequest();
		then_fieldErrorDisplayedFor(FORM_FIELD_USERNAME);
		then_theFormContains(loginWithMissingUserName);
		then_loginIsNotSentToTheService();
	}
	
	@Test
	public void testLoginFormRefusesInputWithMissingPassword() throws Exception {
		given_theUserIsOnTheLoginPage();
		final Login loginWithMissingPassword = new Login("Adam", "12345");
		when_userSubmitsLoginFormContaining(loginWithMissingPassword);
		then_theLoginIsRefusedDueToBadRequest();
		then_fieldErrorNotDispalyedFor(FORM_FIELD_USERNAME);
		then_theFormContains(loginWithMissingPassword);
		then_loginIsSentToTheService(loginWithMissingPassword);
	}
	
	private void then_loginIsSentToTheService(final Login user) throws UserNotExistsException {
		verify(loginService, times(1)).login(user);
	}
	
	private void then_loginIsNotSentToTheService() throws UserNotExistsException {
		verify(loginService, times(0)).login(any());
	}
	
	@Test
	public void testLoginFormWithLeadingAndTrailingTrimmed() throws Exception {
		given_theUserIsOnTheLoginPage();
		when_userSubmitsLoginFormContaining(new Login("  Adam  ", "  12345  "));
		then_loginIsSentToTheService(new Login("Adam", "12345"));
		then_theUserIsRedirectedToTheConfirmationPage();
	}
}
