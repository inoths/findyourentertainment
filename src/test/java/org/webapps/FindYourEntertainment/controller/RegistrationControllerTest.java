package org.webapps.FindYourEntertainment.controller;

import java.util.Locale;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.xpath;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.webapps.FindYourEntertainment.model.User;
import org.webapps.FindYourEntertainment.service.RegistrationService;
import org.webapps.FindYourEntertainment.service.exception.UserAlreadyExistsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

@RunWith(SpringRunner.class)
@WebMvcTest(RegistrationController.class) //indicates which controller will be tested
public class RegistrationControllerTest {

	private static final String PATH_REGISTRATION_FORM = "/registration";
	private static final String PATH_CONFIRMATION_PAGE = "/thank-you";
	
	private static final String FORM_FIELD_USERNAME = "userName";
	private static final String FORM_FIELD_EMAIL = "emailAddress";
	private static final String FORM_FIELD_PASSWORD = "password";
	
	private static final String VIEW_REGISTRATION_FORM = "registration";
	
	private static final String CONTENTTYPE_HTML_UTF8 = "text/html;charset=UTF-8";
	
	@Autowired
	private MockMvc mvc; //served side test support
	
	@MockBean
	private RegistrationService registrationService;
	
	private ResultActions result;
	
	private void given_theUserIsOnTheRegistrationPage() throws Exception {
		// @formatter:off
		result = mvc.perform(
				get(PATH_REGISTRATION_FORM)
				//.accept(MediaType.TEXT_HTML)
				.locale(Locale.ENGLISH))
				
				.andDo(print())				//display for debug
				.andExpect(content().contentType(CONTENTTYPE_HTML_UTF8))
				.andExpect(status().isOk())
				.andExpect(view().name(VIEW_REGISTRATION_FORM));
		// @formatter:on
	}
	
	private void then_theFormContains(final User user) throws Exception {
		// @formatter:off
		result.andExpect(xpath("//input[@name='" + FORM_FIELD_USERNAME + "']/@value").string(user.getUserName()))
		  .andExpect(xpath("//input[@name='" + FORM_FIELD_EMAIL + "']/@value").string(user.getEmailAddress()));
		// @formatter:on
	}
	
	@Test
	public void testRegistrationFormIsEmptyWhenRegistrationFormIsOpened() throws Exception {
		given_theUserIsOnTheRegistrationPage();
		then_theFormContains(new User("", "", "", false));
		then_registrationIsNotSentToTheService();
	}
	
	private void when_userSubmitsRegistrationFormContaining(final User registrationFormInput) throws Exception {
		// @formatter:off
		 result = mvc.perform(
				 post(PATH_REGISTRATION_FORM)
				 .param(FORM_FIELD_USERNAME, registrationFormInput.getUserName())
				 .param(FORM_FIELD_EMAIL, registrationFormInput.getEmailAddress())
				 .param(FORM_FIELD_PASSWORD, registrationFormInput.getPassword()));
		// @formatter:on
	}
	
	private void then_theUserIsRedirectedToTheConfirmationPage() throws Exception {
		result.andDo(print())		//display for debug
			  .andExpect(status().is3xxRedirection())
			  .andExpect(redirectedUrl(PATH_CONFIRMATION_PAGE));
	}
	
	@Test
	public void testRegistrationFormTakesCorrectlySubmittedData() throws Exception {
		given_theUserIsOnTheRegistrationPage();
		User validRegistrationInput = new User("Joker", "joker@mail.com", "12345", false);
		when_userSubmitsRegistrationFormContaining(validRegistrationInput);
		then_registrationIsSentToTheService(validRegistrationInput);
		then_theUserIsRedirectedToTheConfirmationPage();
	}
	
	private void then_theRegistrationIsRefusedDueToBadRequest() throws Exception {
		result.andDo(print())		//for debugging
			  .andExpect(status().isBadRequest())
			  .andExpect(xpath("//div[@class='formErrorMessage']").exists());
	}
	
	private void then_fieldErrorDisplayedFor(String fieldName) throws Exception {
		result.andExpect(xpath("//div[@class='formErrorMessage']/following-sibling::*[position()=1][@name='" + fieldName + "']").exists());
	}
	
	private void then_fieldErrorNotDispalyedFor(String fieldName) throws Exception {
		result.andExpect(xpath("//div[@class='formErrorMessage']/following-sibling::*[position()=1][@name='" + fieldName + "']").doesNotExist());
	}
	
	@Test
	public void testRegistrationFormRefusesInputWithMissingUserName() throws Exception {
		given_theUserIsOnTheRegistrationPage();
		
		final User registrationWithMissingName = new User("", "joker@mail.com", "12345", false);
		when_userSubmitsRegistrationFormContaining(registrationWithMissingName);
		
		then_theRegistrationIsRefusedDueToBadRequest();
		then_fieldErrorDisplayedFor(FORM_FIELD_USERNAME);
		then_fieldErrorNotDispalyedFor(FORM_FIELD_EMAIL);
		then_fieldErrorNotDispalyedFor(FORM_FIELD_PASSWORD);
		then_theFormContains(registrationWithMissingName);
		then_registrationIsNotSentToTheService();
	}
	
	@Test
	public void testRegistrationFormRefusesInputWithMissingEmailAddress() throws Exception {
		given_theUserIsOnTheRegistrationPage();
		
		final User registrationWithMissingEmail = new User("Joker", "", "12345", false);
		when_userSubmitsRegistrationFormContaining(registrationWithMissingEmail);
		
		then_theRegistrationIsRefusedDueToBadRequest();
		then_fieldErrorNotDispalyedFor(FORM_FIELD_USERNAME);
		then_fieldErrorDisplayedFor(FORM_FIELD_EMAIL);
		then_fieldErrorNotDispalyedFor(FORM_FIELD_PASSWORD);
		then_theFormContains(registrationWithMissingEmail);
		then_registrationIsNotSentToTheService();
	}
	
	@Test
	public void testRegistrationFormRefusesInputWithMissingPassword() throws Exception {
		given_theUserIsOnTheRegistrationPage();
		
		final User registrationWithMissingPassword = new User("Joker", "joker@mail.com", "", false);
		when_userSubmitsRegistrationFormContaining(registrationWithMissingPassword);
		
		then_theRegistrationIsRefusedDueToBadRequest();
		then_fieldErrorNotDispalyedFor(FORM_FIELD_USERNAME);
		then_fieldErrorNotDispalyedFor(FORM_FIELD_EMAIL);
		then_fieldErrorDisplayedFor(FORM_FIELD_PASSWORD);
		then_theFormContains(registrationWithMissingPassword);
		then_registrationIsNotSentToTheService();
	}
	
	private void then_registrationIsSentToTheService(final User user)
			throws UserAlreadyExistsException {
		verify(registrationService, times(1)).register(user);
	}
	private void then_registrationIsNotSentToTheService() throws UserAlreadyExistsException {
		verify(registrationService, times(0)).register(any());
	}
	
	@Test
	public void testRegistrationFormWithLeadingAndTrailingTrimmed() throws Exception {
		given_theUserIsOnTheRegistrationPage();
		when_userSubmitsRegistrationFormContaining(new User("  Joker  ", " joker@mail.com  ", " 12345   ", false));
		then_registrationIsSentToTheService(new User("Joker", "joker@mail.com", "12345", false));
		then_theUserIsRedirectedToTheConfirmationPage();
	}
}
