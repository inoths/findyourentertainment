package org.webapps.FindYourEntertainment;

import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.webapps.FindYourEntertainment.model.Login;
import org.webapps.FindYourEntertainment.service.LoginService;
import org.webapps.FindYourEntertainment.service.exception.UserNotExistsException;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LoginValidationTest {
	
	@Autowired
	private LoginService service;
	
	@Test(expected = ConstraintViolationException.class)
	public void test_LoginWithNullUser_fail() throws UserNotExistsException {
		service.login(null);
	}
	
	@Test(expected = ConstraintViolationException.class)
	public void test_LoginWithNameOfSpaceOnly_fail() throws UserNotExistsException {
		service.login(new Login("    ", "12345"));
	}
	
	@Test(expected = ConstraintViolationException.class)
	public void test_LoginWithPasswordOfSpaceOnly_fail() throws UserNotExistsException {
		service.login(new Login("Adam", "     "));
	}
}
