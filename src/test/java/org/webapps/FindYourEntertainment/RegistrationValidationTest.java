package org.webapps.FindYourEntertainment;

import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.webapps.FindYourEntertainment.model.User;
import org.webapps.FindYourEntertainment.service.RegistrationService;
import org.webapps.FindYourEntertainment.service.exception.UserAlreadyExistsException;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RegistrationValidationTest {

	@Autowired
	private RegistrationService service;

	@Test(expected = ConstraintViolationException.class)
	public void test_RegistrationWithNullUser_fail() throws UserAlreadyExistsException {
		service.register(null);
	}

	@Test(expected = ConstraintViolationException.class) // FIXME: except the right exception here
	public void test_RegistrationWithNameOfSpaceOnly_fail() throws UserAlreadyExistsException {
		service.register(new User("     ", "joker@mail.com", "12345", false));
	}

	@Test(expected = ConstraintViolationException.class) // FIXME: except the right exception here
	public void test_RegistrationWithPasswordOfSpaceOnly_fail() throws UserAlreadyExistsException {
		service.register(new User("Joker", "joker@mail.com", "     ", false));
	}

	@Test(expected = ConstraintViolationException.class) // FIXME: except the right exception here
	public void test_RegistrationWithEmailOfSpaceOnly_fail() throws UserAlreadyExistsException {
		service.register(new User("Joker", "      ", "12345", false));
	}

	@Test(expected = ConstraintViolationException.class) // FIXME: except the right exception here
	public void test_RegistrationWithIncorrectEmailType_fail() throws UserAlreadyExistsException {
		service.register(new User("Joker", "joker()mail.com", "12345", false));
	}
}
