package org.webapps.FindYourEntertainment;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.webapps.FindYourEntertainment.model.Login;
import org.webapps.FindYourEntertainment.model.User;
import org.webapps.FindYourEntertainment.service.LoginService;
import org.webapps.FindYourEntertainment.service.RegistrationService;
import org.webapps.FindYourEntertainment.service.exception.UserAlreadyExistsException;
import org.webapps.FindYourEntertainment.service.exception.UserNotExistsException;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FindYourEntertainmentApplicationTests {
	
	@Autowired
	private RegistrationService registrationService;
	
	@Autowired
	private LoginService loginService;

	@Test
	public void test_RegistrationWithValidUser_successful() throws UserAlreadyExistsException {
		registrationService.register(new User("Joker", "joker@mail.com", "12345", false));
	}
	
	@Test
	public void test_LoginWithValidUser_successful() throws UserNotExistsException {
		loginService.login(new Login("Adam", "12345"));
	}

}
