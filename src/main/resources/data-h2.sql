INSERT INTO users (userID, userName, eMail, password, isAdmin)
VALUES
('82467412-9d97-11e8-98d0-529269fb1459', 'Adam', 'adam@mail.com', '12345', 0),
('8246780e-9d97-11e8-98d0-529269fb1459', 'Random', 'random@mail.com', '12345', 0),
('82467958-9d97-11e8-98d0-529269fb1459', 'Janos', 'janos@mail.com', '12345', 0);

INSERT INTO movies (movieName, releaseDate, directorName, movieImage, shortDescription)
VALUES
('Iron Man', '2008', 'Jon Favreau', 'ironman.jpg', 'Iron Man!!!'),
('Iron Man2', '2010', 'Jon Favreau', 'ironman2.jpg', 'Iron Man2!!!'),
('Iron Man3', '2013', 'Shane Black', 'ironman3.jpg', 'Iron Man3!!!');

INSERT INTO games (gameName, releaseDate, companyName, gameImage, shortDescription)
VALUES
('Guild Wars 2', '2012', 'ArenaNet', 'guildwars2.jpg', 'For the legion!'),
('Elder Scrols V: Skyrim', '2011', 'Bethesda', 'skyrim.jpg', 'Best game ever made!'),
('Stellaris', '2016', 'Paradox Interactive', 'stellaris.jpg', 'Conquer the universe!');