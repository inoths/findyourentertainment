CREATE TABLE IF NOT EXISTS users (
	userID VARCHAR(36) PRIMARY KEY,
	userName VARCHAR(64) NOT NULL,
	eMail VARCHAR(64) NOT NULL,
	password VARCHAR(64) NOT NULL,
	isAdmin BIT NOT NULL,
	UNIQUE KEY UK_userID (userID),
	UNIQUE KEY UK_userName (userName),
	UNIQUE KEY UK_eMail (eMail)
);

CREATE TABLE IF NOT EXISTS movies (
	ID INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	movieName VARCHAR(64) NOT NULL,
	releaseDate VARCHAR(36) NOT NULL,
	directorName VARCHAR(64) NOT NULL,
	movieImage VARCHAR(128) NOT NULL,
	shortDescription TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS games (
	ID INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	gameName VARCHAR(64) NOT NULL,
	releaseDate VARCHAR(36) NOT NULL,
	companyName VARCHAR(64) NOT NULL,
	gameImage VARCHAR(128) NOT NULL,
	shortDescription TEXT NOT NULL
);