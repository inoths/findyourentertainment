package org.webapps.FindYourEntertainment.service;

import org.springframework.validation.annotation.Validated;
import org.webapps.FindYourEntertainment.model.Game;

/**
 * Interface of Game searching service.
 */
@Validated
public interface GameSearchService {
	/**
	 * Get a new Game.
	 * 
	 * @return Game model
	 */
	Game getNewGame();
	/**
	 * Get a random number between 1 and num.
	 * 
	 * @param num integer number
	 * @return an integer number
	 */
	int calcARandomRecord(int num);
}
