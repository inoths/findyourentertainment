package org.webapps.FindYourEntertainment.service;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.springframework.validation.annotation.Validated;
import org.webapps.FindYourEntertainment.model.Login;
import org.webapps.FindYourEntertainment.service.exception.UserNotExistsException;

/**
 * Interface of Login service.
 */
@Validated
public interface LoginService {
	
	/**
	 * Send the user to the correct repository.
	 * 
	 * @param user Login model.
	 * @return true if the login was successful and false if it wasn't.
	 * @throws UserNotExistsException
	 */
	boolean login(@NotNull @Valid Login user) throws UserNotExistsException;
}
