package org.webapps.FindYourEntertainment.service;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.springframework.validation.annotation.Validated;
import org.webapps.FindYourEntertainment.model.User;
import org.webapps.FindYourEntertainment.service.exception.UserAlreadyExistsException;

/**
 * Interface of the Registration service.
 */
@Validated
public interface RegistrationService {

	/**
	 * Send a User model the the correct repository.
	 * 
	 * @param user User model.
	 * @throws UserAlreadyExistsException
	 */
	void register(@NotNull @Valid User user) throws UserAlreadyExistsException;
}
