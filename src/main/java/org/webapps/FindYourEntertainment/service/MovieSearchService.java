package org.webapps.FindYourEntertainment.service;

import org.springframework.validation.annotation.Validated;
import org.webapps.FindYourEntertainment.model.Movie;

/**
 * Interface of Movie searching service.
 */
@Validated
public interface MovieSearchService {
	/**
	 * Get a new movie.
	 * 
	 * @return Movie model
	 */
	Movie getNewMovie();
	/**
	 * Get a random number between 1 and num.
	 * 
	 * @param num an integer number
	 * @return an integer number
	 */
	int calcARandomRecord(int num);
}
