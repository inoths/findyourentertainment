package org.webapps.FindYourEntertainment.service.exception;

/**
 * An exception that will thrown when the user is not in the database.
 */
public class UserNotExistsException extends Exception {
	
	/**
	 * @param message the error message
	 */
	public UserNotExistsException(String message) {
		super(message);
	}
}
