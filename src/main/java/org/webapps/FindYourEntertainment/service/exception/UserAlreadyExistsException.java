package org.webapps.FindYourEntertainment.service.exception;

/**
 * An exception that will thrown when the user is already in the database.
 */
public class UserAlreadyExistsException extends Exception {
	
	/**
	 * @param message the error messeage
	 */
	public UserAlreadyExistsException(String message) {
		super(message);
	}
}
