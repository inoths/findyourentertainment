package org.webapps.FindYourEntertainment.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.webapps.FindYourEntertainment.model.User;
import org.webapps.FindYourEntertainment.repository.RegistrationRepository;
import org.webapps.FindYourEntertainment.service.RegistrationService;
import org.webapps.FindYourEntertainment.service.exception.UserAlreadyExistsException;

/**
 * Registration service implementation
 */
@Service
public class RegistrationServiceImpl implements RegistrationService {
	
	private Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private RegistrationRepository repo;
	
	@Override
	@Transactional(rollbackFor = UserAlreadyExistsException.class)
	public void register(User user) throws UserAlreadyExistsException {
		
		logger.info("Registering user" + user);
		
		try {
			repo.save(user);
		} catch (DuplicateKeyException duplicate) {
			logger.error("Registering was unsuccesfull with " + duplicate);
			throw new UserAlreadyExistsException("User already exists with name: " + user.getUserName());
		}
	}

}