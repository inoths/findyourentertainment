package org.webapps.FindYourEntertainment.service.impl;

import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.webapps.FindYourEntertainment.model.Game;
import org.webapps.FindYourEntertainment.repository.GameSearchRepository;
import org.webapps.FindYourEntertainment.service.GameSearchService;

/**
 * 	Game searching service implementation
 */
@Service
public class GameSearchServiceImpl implements GameSearchService {
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private GameSearchRepository repo;

	@Override
	public Game getNewGame() {
		
		logger.info("Asking the repo for a new game");
		
		return repo.getARecordByIndex(calcARandomRecord(repo.getNumberOfRecords()));
	}

	@Override
	public int calcARandomRecord(int num) {
		
		logger.info("Generating a new random number between 1 and " + num);
		
		Random random = new Random();
		return random.nextInt(num) + 1;
	}

}
