package org.webapps.FindYourEntertainment.service.impl;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.webapps.FindYourEntertainment.model.Login;
import org.webapps.FindYourEntertainment.repository.LoginRepository;
import org.webapps.FindYourEntertainment.service.LoginService;
import org.webapps.FindYourEntertainment.service.exception.UserNotExistsException;

/**
 * Login service implementation
 */
@Service
public class LoginServiceImpl implements LoginService {
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private LoginRepository repo;

	@Override
	public boolean login(@NotNull @Valid Login user) throws UserNotExistsException {
		
		logger.info("Logging in user: {}" + user);
		
	 	return repo.check(user);
	}

}
