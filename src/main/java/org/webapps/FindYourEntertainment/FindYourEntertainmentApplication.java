package org.webapps.FindYourEntertainment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FindYourEntertainmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(FindYourEntertainmentApplication.class, args);
	}
}
