package org.webapps.FindYourEntertainment.controller;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.webapps.FindYourEntertainment.model.Login;
import org.webapps.FindYourEntertainment.service.LoginService;
import org.webapps.FindYourEntertainment.service.exception.UserNotExistsException;

/**
 * This controller is responsible for login already existing users.
 */
@Controller
public class LoginController {

	private static final String PATH_LOGIN = "/login";

	private final Logger logger = LoggerFactory.getLogger(getClass());

	public boolean LoggedIn = false;

	@Autowired
	private LoginService loginService;

	/**
	 * Get to the login page.
	 * 
	 * @param u a login model
	 * @return login view
	 */
	@GetMapping(PATH_LOGIN)
	public String getToLoginPage(@ModelAttribute("loginForm") Login u) {
		return "login";
	}

	/**
	 * Submit login and send to the service, and send to service answer to the correct view.
	 * 
	 * @param user a login model
	 * @return the correct view after the user.
	 * @throws UserNotExistsException a not used exception
	 */
	@PostMapping(PATH_LOGIN)
	public String submitLogin(@ModelAttribute("loginForm") @Valid Login user, BindingResult result,
			HttpServletResponse response) throws UserNotExistsException {

		logger.debug("user submitted this login with this data: {}" + user);

		if (result.hasErrors()) {
			response.setStatus(HttpStatus.BAD_REQUEST.value());
			result.reject("loginForm.error.incompleteInput");
			return "login";
		} else {
			if (loginService.login(user) != true) {
				response.setStatus(HttpStatus.BAD_REQUEST.value());
				result.reject("loginForm.error.incompleteInput");
				return "login";
			} else {
				LoggedIn = true;
				return "redirect:/thank-you";
			}
		}
	}

	/**
	 * ExceptionHandler.
	 * 
	 * @param e exception
	 * @return login view
	 */
	@ExceptionHandler
	@ResponseStatus(HttpStatus.CONFLICT)
	public String userNotExists(UserNotExistsException e) {
		return "login";
	}

	/**
	 * Trimmer.
	 * 
	 * @param binder the binder, that will help trimming the input
	 */
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		StringTrimmerEditor trimmer = new StringTrimmerEditor(false);
		binder.registerCustomEditor(String.class, trimmer);
	}
}
