package org.webapps.FindYourEntertainment.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.webapps.FindYourEntertainment.model.Movie;
import org.webapps.FindYourEntertainment.service.MovieSearchService;

/**
 * Movie searching controller.
 */
@Controller
public class MovieSearchController {
	
	private static final String PATH_MOVIE = "/movie";
	
	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private MovieSearchService movieSearchService;
	
	/**
	 * Search for a new movie and send it to the correct view.
	 * 
	 * @param model A model that will bring attributes to the view
	 * @return movie view
	 */
	@GetMapping(PATH_MOVIE)
	public String getToTheMovieSearchPage(Model model) {
		
		logger.info("Getting a new movie");
		
		Movie movie = new Movie(); 
	 	movie = movieSearchService.getNewMovie();
	 	model.addAttribute("movieImage", movie.getMovieImage());
	 	model.addAttribute("movieName", movie.getMovieName());
	 	model.addAttribute("directorName", movie.getDirectorName());
	 	model.addAttribute("releaseDate", movie.getReleaseDate());
	 	model.addAttribute("shortDescription", movie.getShortDescription());
	 	
	 	logger.info("Got a new movie: {}" + movie);
	 	
		return "movie";
	}
}
