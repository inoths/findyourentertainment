package org.webapps.FindYourEntertainment.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.webapps.FindYourEntertainment.model.Game;
import org.webapps.FindYourEntertainment.service.GameSearchService;

/**
 * Game searching controller.
 */
@Controller
public class GameSearchController {

	private static final String PATH_GAME = "/game";

	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private GameSearchService gameSearchService;

	/**
	 * Search for a new game and send it to the correct view.
	 * 
	 * @param model A model that will bring attributes to the view
	 * @return game view
	 */
	@GetMapping(PATH_GAME)
	public String getToTheGameSearchPage(Model model) {

		logger.info("Getting a new game");

		Game game = new Game();
		game = gameSearchService.getNewGame();
		model.addAttribute("gameImage", game.getGameImage());
		model.addAttribute("gameName", game.getGameName());
		model.addAttribute("companyName", game.getCompanyName());
		model.addAttribute("releaseDate", game.getReleaseDate());
		model.addAttribute("shortDescription", game.getShortDescription());

		logger.info("Got a new movie: {}" + game);

		return "game";
	}
}
