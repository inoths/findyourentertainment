package org.webapps.FindYourEntertainment.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * A controller that bring you back to the confirmation page.
 */
@Controller
public class ConfirmationController {
	/**
	 * Returns the user to the controller view.
	 * 
	 * @return the thank-you view
	 */
	@GetMapping("/thank-you")
	public String getBackAfterRegistration() {
		return "thank-you";
	}
}
