package org.webapps.FindYourEntertainment.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * This controller is the main controller.
 */
@Controller
public class MainController {

	@Autowired
	private LoginController loginController;

	/**
	 * get to the main page.
	 * 
	 * @return main view
	 */
	@GetMapping("/")
	public String getToMainPage() {
		return "main";
	}

	/**
	 * Get back to the thank-you view (logged in)
	 * 
	 * @return thank-you view
	 */
	@GetMapping("/back")
	public String getBackToMain() {
		return "thank-you";
	}

	/**
	 * Logging out.
	 * 
	 * @return main view
	 */
	@GetMapping("/logout")
	public String getLoggedOut() {
		loginController.LoggedIn = false;
		return "main";
	}
}
