package org.webapps.FindYourEntertainment.controller;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.webapps.FindYourEntertainment.model.User;
import org.webapps.FindYourEntertainment.service.RegistrationService;
import org.webapps.FindYourEntertainment.service.exception.UserAlreadyExistsException;

/**
 * This controller is responsible for creating new user.
 */
@Controller
public class RegistrationController {

	private static final String PATH_REGISTRATION = "/registration";

	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private RegistrationService registrationService;

	/**
	 * Get to user to the registration page.
	 * 
	 * @param u User model
	 * @return registration view
	 */
	@GetMapping(PATH_REGISTRATION)
	public String getToRegistrationPage(@ModelAttribute("registrationForm") User u) {
		return "registration";
	}

	/**
	 * Submit registration and send it to the service, and the service respond to the correct view.
	 * 
	 * @param user User model
	 * @return the correct view
	 * @throws UserAlreadyExistsException an exception that defines what will happen if the user is already in the database
	 */
	@PostMapping(PATH_REGISTRATION)
	public String submitRegistration(@ModelAttribute("registrationForm") @Valid User user, BindingResult result,
			HttpServletResponse response) throws UserAlreadyExistsException {

		logger.debug("user submitted this registration with this data: {}", user);

		if (result.hasErrors()) {
			response.setStatus(HttpStatus.BAD_REQUEST.value());
			result.reject("registrationForm.error.incompleteInput");
			return "registration";
		} else {
			user.setAdmin(false);
			registrationService.register(user);
			return "redirect:/thank-you";
		}
	}

	/**
	 * ExceptionHandler.
	 * 
	 * @param e exception
	 * @return error/error-user-already-exists view
	 */
	@ExceptionHandler
	@ResponseStatus(HttpStatus.CONFLICT)
	public String userAlreadyExists(UserAlreadyExistsException e) {
		return "error/error-user-already-exists";
	}

	/**
	 * Trimmer.
	 * 
	 * @param binder the binder, that will help trimming the input
	 */
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		StringTrimmerEditor trimmer = new StringTrimmerEditor(false);
		binder.registerCustomEditor(String.class, trimmer);
	}
}
