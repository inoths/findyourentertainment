package org.webapps.FindYourEntertainment.model;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * Defines the login model.
 */
public class Login {
	
	@NotBlank
	@Size(min = 4, max = 20)
	private String userName;
	@NotBlank
	@Size(min = 3, max = 20)
	private String password;
	/**
	 * Constructor without parameters.
	 */
	public Login() {
	}
	/**
	 * Constructor with parameters.
	 * 
	 * @param userName a String
	 * @param password a String
	 */
	public Login(String userName, String password) {
		this.userName = userName;
		this.password = password;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		result = prime * result + ((userName == null) ? 0 : userName.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Login other = (Login) obj;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (userName == null) {
			if (other.userName != null)
				return false;
		} else if (!userName.equals(other.userName))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Login [userName=" + userName + ", password=" + password + "]";
	}
	/**
	 * Get the name of the user.
	 *  
	 * @return a String
	 */
	public String getUserName() {
		return userName;
	}
	/**
	 * Sets the name of the user.
	 * 
	 * @param userName a String
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
	/**
	 * Get the password of the user.
	 * 
	 * @return a String
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * Sets the password of the user.
	 * 
	 * @param password a String
	 */
	public void setPassword(String password) {
		this.password = password;
	}
}
