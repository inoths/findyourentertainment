package org.webapps.FindYourEntertainment.model;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * Defines the User
 */
public class User {

	@NotBlank
	@Size(min = 4, max = 20)
	private String userName;		//Name of the user
	@NotBlank
	@Email
	private String emailAddress;	//Email of the user
	@NotBlank
	@Size(min = 3, max = 20)
	private String password;		//Password of the user's accounts
	private boolean isAdmin = false;//True if the account is administrator, false as default

	/** 
	 * Constructor without parameter.
	 */
	public User() {
		//Do nothing on purpose.
	}

	/**
	 * Constructor to create object of the Registration class.
	 * 
	 * @param userName		Name of the user
	 * @param emailAddress	Email of the user
	 * @param password		Password of the user
	 * @param isAdmin		True if the user is administrator
	 */
	public User(String userName, String emailAddress, String password, boolean isAdmin) {
		this.userName = userName;
		this.emailAddress = emailAddress;
		this.password = password;
		this.isAdmin = isAdmin;
	}

	//Use that of you want to log a user account informations
	@Override
	public String toString() {
		return "Registration [userName=" + userName + ", emailAddress=" + emailAddress + ", password=" + password
				+ ", isAdmin=" + isAdmin + "]";
	}

	/**
	 * Returns the name of the user.
	 * 
	 * @return	the name of the user as a string
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * Sets the name of the user.
	 * 
	 * @param userName	a string
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * Returns the email of the user.
	 * 
	 * @return	the email address as a string
	 */
	public String getEmailAddress() {
		return emailAddress;
	}

	/**
	 * Sets the email of the user.
	 * 
	 * @param emailAddress	a string with a @ in it
	 */
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	/**
	 * Returns a string that is the password of the user.
	 * The password must be between 3 and 15 characters. And it can't contain special characters like @ or [].
	 * 
	 * @return a string
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Sets the password of the user.
	 * 
	 * @param password	The password must be between 3 and 15 characters. 
	 * And it can't contain special characters like @ or []
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * Returns the state of the user's account.
	 * 
	 * @return	the privileges of the user
	 */
	public boolean isAdmin() {
		return isAdmin;
	}

	/**
	 * Sets the user privileges.
	 * 
	 * @param isAdmin a boolean that is true if the user is admin
	 */
	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((emailAddress == null) ? 0 : emailAddress.hashCode());
		result = prime * result + (isAdmin ? 1231 : 1237);
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		result = prime * result + ((userName == null) ? 0 : userName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (emailAddress == null) {
			if (other.emailAddress != null)
				return false;
		} else if (!emailAddress.equals(other.emailAddress))
			return false;
		if (isAdmin != other.isAdmin)
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (userName == null) {
			if (other.userName != null)
				return false;
		} else if (!userName.equals(other.userName))
			return false;
		return true;
	}
}
