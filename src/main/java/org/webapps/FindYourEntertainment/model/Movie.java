package org.webapps.FindYourEntertainment.model;

/**
 * Movie model.
 */
public class Movie {

	private String movieName;
	private String releaseDate; // The date when the movie is released
	private String directorName;
	private String movieImage; // Name and extension of the image file, under resources/static/images/movies/
	private String shortDescription;

	/**
	 * Constructor without parameters.
	 */
	public Movie() {

	}

	/**
	 * Constructor with parameters.
	 * 
	 * @param movieName a String that defines the name of the movie
	 * @param releaseDate a String that defines the release date of the movie
	 * @param directorName a String that defines the name of the director that created the movie
	 * @param movieImage a String that gives the name and extension of the image in the image folder
	 * @param shortDescription a String that gives a short description of the movie
	 */
	public Movie(String movieName, String releaseDate, String directorName, String movieImage,
			String shortDescription) {

		this.movieName = movieName;
		this.releaseDate = releaseDate;
		this.directorName = directorName;
		this.movieImage = movieImage;
		this.shortDescription = shortDescription;
	}

	@Override
	public String toString() {
		return "Movie [movieName=" + movieName + ", releaseDate=" + releaseDate + ", directorName=" + directorName
				+ ", movieImage=" + movieImage + ", shortDescription=" + shortDescription + "]";
	}

	/**
	 * Gets the name of the movie.
	 * 
	 * @return a String
	 */
	public String getMovieName() {
		return movieName;
	}

	/**
	 * Sets the name of the movie.
	 * 
	 * @param movieName a String
	 */
	public void setMovieName(String movieName) {
		this.movieName = movieName;
	}

	/**
	 * Release date of the movie as a String (for example: 2010).
	 * 
	 * @return a String
	 */
	public String getReleaseDate() {
		return releaseDate;
	}

	/**
	 * Sets the release data of the movie as a String (for example: 2010).
	 * 
	 * @param releaseDate a String
	 */
	public void setReleaseDate(String releaseDate) {
		this.releaseDate = releaseDate;
	}

	/**
	 * Gets the name of the director that created the movie.
	 * 
	 * @return a String
	 */
	public String getDirectorName() {
		return directorName;
	}

	/**
	 * Sets the name of the director that created the movie.
	 * 
	 * @param directorName a String
	 */
	public void setDirectorName(String directorName) {
		this.directorName = directorName;
	}

	/**
	 * Gets the image of the movie (for example: bestMovie.png).
	 * 
	 * @return a String
	 */
	public String getMovieImage() {
		return movieImage;
	}

	/**
	 * Sets the image of the movie (for example: bestMovie.png).
	 * 
	 * @param movieImage a String
	 */
	public void setMovieImage(String movieImage) {
		this.movieImage = movieImage;
	}

	/**
	 * Get the short description of the movie.
	 * 
	 * @return a String
	 */
	public String getShortDescription() {
		return shortDescription;
	}

	/**
	 * Sets the short description of the movie.
	 * 
	 * @param shortDescription a String
	 */
	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((directorName == null) ? 0 : directorName.hashCode());
		result = prime * result + ((movieImage == null) ? 0 : movieImage.hashCode());
		result = prime * result + ((movieName == null) ? 0 : movieName.hashCode());
		result = prime * result + ((releaseDate == null) ? 0 : releaseDate.hashCode());
		result = prime * result + ((shortDescription == null) ? 0 : shortDescription.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Movie other = (Movie) obj;
		if (directorName == null) {
			if (other.directorName != null)
				return false;
		} else if (!directorName.equals(other.directorName))
			return false;
		if (movieImage == null) {
			if (other.movieImage != null)
				return false;
		} else if (!movieImage.equals(other.movieImage))
			return false;
		if (movieName == null) {
			if (other.movieName != null)
				return false;
		} else if (!movieName.equals(other.movieName))
			return false;
		if (releaseDate == null) {
			if (other.releaseDate != null)
				return false;
		} else if (!releaseDate.equals(other.releaseDate))
			return false;
		if (shortDescription == null) {
			if (other.shortDescription != null)
				return false;
		} else if (!shortDescription.equals(other.shortDescription))
			return false;
		return true;
	}

}
