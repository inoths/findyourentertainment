package org.webapps.FindYourEntertainment.model;

/**
 * Game model.
 */
public class Game {

	private String gameName;
	private String releaseDate; // The date when the game is released
	private String companyName;
	private String gameImage; // Name and extension of the image file, under resources/static/images/games/
	private String shortDescription;

	/**
	 * Constructor without parameters.
	 */
	public Game() {

	}

	/**
	 * Constructor with parameters.
	 * 
	 * @param gameName a String that defines the name of the game
	 * @param releaseDate a String that defines the release date of the game
	 * @param companyName a String that defines the name of the company that released the game
	 * @param gameImage a String that gives the name and extension of the image in the image folder
	 * @param shortDescription a String that gives a short description of the game
	 */
	public Game(String gameName, String releaseDate, String companyName, String gameImage, String shortDescription) {
		this.gameName = gameName;
		this.releaseDate = releaseDate;
		this.companyName = companyName;
		this.gameImage = gameImage;
		this.shortDescription = shortDescription;
	}

	@Override
	public String toString() {
		return "Game [gameName=" + gameName + ", releaseDate=" + releaseDate + ", companyName=" + companyName
				+ ", gameImage=" + gameImage + ", shortDescription=" + shortDescription + "]";
	}

	/**
	 * Gets the name of the game.
	 * 
	 * @return a String
	 */
	public String getGameName() {
		return gameName;
	}

	/**
	 * Sets the name of the game.
	 * 
	 * @param gameName a String
	 */
	public void setGameName(String gameName) {
		this.gameName = gameName;
	}

	/**
	 * Release date of the game as a String (for example: 2010).
	 * 
	 * @return a String
	 */
	public String getReleaseDate() {
		return releaseDate;
	}

	/**
	 * Sets the release data of the game as a String (for example: 2010).
	 * 
	 * @param releaseDate a String
	 */
	public void setReleaseDate(String releaseDate) {
		this.releaseDate = releaseDate;
	}

	/**
	 * Gets the name of the company that created the game.
	 * 
	 * @return a String
	 */
	public String getCompanyName() {
		return companyName;
	}

	/**
	 * Sets the name of the company that created the game.
	 * 
	 * @param companyName a String
	 */
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	/**
	 * Gets the image of the game (for example: bestGame.png).
	 * 
	 * @return a String
	 */
	public String getGameImage() {
		return gameImage;
	}

	/**
	 * Sets the image of the game (for example: bestGame.png).
	 * 
	 * @param gameImage a String
	 */
	public void setGameImage(String gameImage) {
		this.gameImage = gameImage;
	}

	/**
	 * Get the short description of the game.
	 * 
	 * @return a String
	 */
	public String getShortDescription() {
		return shortDescription;
	}

	/**
	 * Sets the short description of the game.
	 * 
	 * @param shortDescription a String
	 */
	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((companyName == null) ? 0 : companyName.hashCode());
		result = prime * result + ((gameImage == null) ? 0 : gameImage.hashCode());
		result = prime * result + ((gameName == null) ? 0 : gameName.hashCode());
		result = prime * result + ((releaseDate == null) ? 0 : releaseDate.hashCode());
		result = prime * result + ((shortDescription == null) ? 0 : shortDescription.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Game other = (Game) obj;
		if (companyName == null) {
			if (other.companyName != null)
				return false;
		} else if (!companyName.equals(other.companyName))
			return false;
		if (gameImage == null) {
			if (other.gameImage != null)
				return false;
		} else if (!gameImage.equals(other.gameImage))
			return false;
		if (gameName == null) {
			if (other.gameName != null)
				return false;
		} else if (!gameName.equals(other.gameName))
			return false;
		if (releaseDate == null) {
			if (other.releaseDate != null)
				return false;
		} else if (!releaseDate.equals(other.releaseDate))
			return false;
		if (shortDescription == null) {
			if (other.shortDescription != null)
				return false;
		} else if (!shortDescription.equals(other.shortDescription))
			return false;
		return true;
	}
}
