package org.webapps.FindYourEntertainment.repository.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.webapps.FindYourEntertainment.model.Game;
import org.webapps.FindYourEntertainment.repository.GameSearchRepository;

/**
 * Game searching repository implementation.
 */
@Repository
public class GameSearchRepositoryImpl implements GameSearchRepository {
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	String name;
	String date;
	String cName;
	String mImage;
	String shortD;

	@Override
	public int getNumberOfRecords() {
		
		logger.info("Asking for a new number of records");
		
		final String sqlNumberOfIndexes = "SELECT MAX(ID) FROM games";
		
		return jdbcTemplate.queryForObject(sqlNumberOfIndexes, Integer.class);
	}

	@Override
	public Game getARecordByIndex(int index) {
		
		Game game = new Game();
		
		final String sqlgameName = " SELECT gameName FROM games WHERE ID=" + index;
		final String sqlreleaseDate = " SELECT releaseDate FROM games WHERE ID=" + index;
		final String sqlcompanyName = " SELECT companyName FROM games WHERE ID=" + index;
		final String sqlgameImage = " SELECT gameImage FROM games WHERE ID=" + index;
		final String sqlshortDescription = " SELECT shortDescription FROM games WHERE ID=" + index;
		
		game.setGameName(jdbcTemplate.queryForObject(sqlgameName, String.class));
		game.setReleaseDate(jdbcTemplate.queryForObject(sqlreleaseDate, String.class));
		game.setCompanyName(jdbcTemplate.queryForObject(sqlcompanyName, String.class));
		game.setGameImage(jdbcTemplate.queryForObject(sqlgameImage, String.class));
		game.setShortDescription(jdbcTemplate.queryForObject(sqlshortDescription, String.class));
		
		logger.info("Got a new movie from db, sending it to the service: {}" + game);
		
		return game;
	}

}
