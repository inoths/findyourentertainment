package org.webapps.FindYourEntertainment.repository.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.webapps.FindYourEntertainment.model.Movie;
import org.webapps.FindYourEntertainment.repository.MovieSearchRepository;

/**
 * Movie searching repository implementation.
 */
@Repository
public class MovieSearchRepositoryImpl implements MovieSearchRepository {
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	String name;
	String date;
	String dName;
	String mImage;
	String shortD;

	@Override
	public int getNumberOfRecords() {
		
		logger.info("Asking for a new number of records");
		
		final String sqlNumberOfIndexes = "SELECT MAX(ID) FROM movies";
		
		return jdbcTemplate.queryForObject(sqlNumberOfIndexes, Integer.class);
	}

	@Override
	public Movie getARecordByIndex(int index) {
		
		Movie movie = new Movie();
		
		final String sqlmovieName = " SELECT movieName FROM movies WHERE ID=" + index;
		final String sqlreleaseDate = " SELECT releaseDate FROM movies WHERE ID=" + index;
		final String sqldirectorName = " SELECT directorName FROM movies WHERE ID=" + index;
		final String sqlmovieImage = " SELECT movieImage FROM movies WHERE ID=" + index;
		final String sqlshortDescription = " SELECT shortDescription FROM movies WHERE ID=" + index;
		
		movie.setMovieName(jdbcTemplate.queryForObject(sqlmovieName, String.class));
		movie.setReleaseDate(jdbcTemplate.queryForObject(sqlreleaseDate, String.class));
		movie.setDirectorName(jdbcTemplate.queryForObject(sqldirectorName, String.class));
		movie.setMovieImage(jdbcTemplate.queryForObject(sqlmovieImage, String.class));
		movie.setShortDescription(jdbcTemplate.queryForObject(sqlshortDescription, String.class));
		
		logger.info("Got a new movie from db, sending it to the service: {}" + movie);
		
		return movie;
	}

}
