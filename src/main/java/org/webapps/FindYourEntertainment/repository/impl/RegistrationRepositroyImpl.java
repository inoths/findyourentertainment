package org.webapps.FindYourEntertainment.repository.impl;

import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.webapps.FindYourEntertainment.model.User;
import org.webapps.FindYourEntertainment.repository.RegistrationRepository;

/**
 * Registration repository implementation.
 */
@Repository
public class RegistrationRepositroyImpl implements RegistrationRepository {

	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	private byte isAdminAsBit;
	
	@Override
	public void save(User user) {
		logger.info("saves user now: " + user);
		
		final String sql = "INSERT INTO users (userID, userName, eMail, password, isAdmin) VALUES (?, ?, ?, ?, ?)";
		final String userID = UUID.randomUUID().toString();
		
		if(user.isAdmin() == true) {
			isAdminAsBit = 1;
		} else {
			isAdminAsBit = 0;
		}
		
		jdbcTemplate.update(sql, userID, user.getUserName(), user.getEmailAddress(), user.getPassword(), isAdminAsBit);
	}

}
