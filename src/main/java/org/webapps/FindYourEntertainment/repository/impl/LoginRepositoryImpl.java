package org.webapps.FindYourEntertainment.repository.impl;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.webapps.FindYourEntertainment.model.Login;
import org.webapps.FindYourEntertainment.repository.LoginRepository;

/**
 * Login repository implementation.
 */
@Repository
public class LoginRepositoryImpl implements LoginRepository {
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public boolean check(@NotNull @Valid Login user) {
		
		logger.info("Checking user now: {}" + user);
		
		String name;
		String passWord;
		
		try {
			final String sqlUserName = "SELECT userName FROM users WHERE userName='" + user.getUserName() + "'";
			final String sqlPassword = "SELECT password FROM users WHERE userName='" + user.getUserName() + "'";
			
			name = jdbcTemplate.queryForObject(sqlUserName, String.class);
			passWord = jdbcTemplate.queryForObject(sqlPassword, String.class);
			
			if(name.equals(user.getUserName()) && passWord.equals(user.getPassword())) {
				return true;
			} else {
				return false;
			}
		} catch (EmptyResultDataAccessException e) {
			return false;
		}
	}

}
