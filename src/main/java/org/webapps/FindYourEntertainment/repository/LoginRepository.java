package org.webapps.FindYourEntertainment.repository;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;
import org.webapps.FindYourEntertainment.model.Login;

/**
 * Interfaces of the Login repository.
 */
@Validated
public interface LoginRepository {
	
	/**
	 * Check the user to allow the login.
	 * 
	 * @param user Login
	 * @return true if the user is allowed to login and false if it's not allowed
	 */
	boolean check(@NotNull @Valid Login user);
}
