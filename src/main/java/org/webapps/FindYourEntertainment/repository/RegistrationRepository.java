package org.webapps.FindYourEntertainment.repository;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;
import org.webapps.FindYourEntertainment.model.User;

/**
 * Interface of the Registration repository.
 */
@Validated
public interface RegistrationRepository {

	/**
	 * Save the user to the database.
	 * 
	 * @param user User model
	 */
	void save(@NotNull @Valid User user);
}
