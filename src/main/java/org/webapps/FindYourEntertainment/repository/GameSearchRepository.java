package org.webapps.FindYourEntertainment.repository;

import org.springframework.validation.annotation.Validated;
import org.webapps.FindYourEntertainment.model.Game;

/**
 * Interface of the Game searching repository.
 */
@Validated
public interface GameSearchRepository {
	/**
	 * Get the number of Records in the database.
	 * 
	 * @return integer maximum number of records
	 */
	int getNumberOfRecords();
	/**
	 * Get a Game by it's index.
	 * 
	 * @param index integer index
	 * @return Game model
	 */
	Game getARecordByIndex(int index);
}
