package org.webapps.FindYourEntertainment.repository;

import org.springframework.validation.annotation.Validated;
import org.webapps.FindYourEntertainment.model.Movie;

/**
 * Interface of the Movie searching repository.
 */
@Validated
public interface MovieSearchRepository {
	/**
	 * Get the number of Records in the database.
	 * 
	 * @return integer maximum number of records
	 */
	int getNumberOfRecords();
	/**
	 * Get a Movie by it's index.
	 * 
	 * @param index integer index
	 * @return Movie model
	 */
	Movie getARecordByIndex(int index);
}
